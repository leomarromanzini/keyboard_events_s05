let boxTop = 200;
let boxLeft = 200;


document.addEventListener('keydown', (event) => {

    const keyName = event.key;

    if(keyName === "w" || keyName === "ArrowUp"){

       boxTop -= 15;
    }

    if(keyName === "s" || keyName === "ArrowDown"){

        boxTop += 15;
     }

     if(keyName === "a" || keyName === "ArrowLeft"){

        boxLeft -= 15;
     }

     if(keyName === "d" || keyName === "ArrowRight"){

        boxLeft += 15;
     }

    document.getElementById("box").style.top = boxTop + "px";
    document.getElementById("box").style.left = boxLeft + "px";
   
    console.log(boxTop)
    console.log(boxLeft)
  });
 